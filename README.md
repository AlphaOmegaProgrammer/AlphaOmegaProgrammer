# About

I've been progamming professionally since around the end of 2011, and I self-studied programming long before that. My career has been in full stack web dev primarily using PHP, however I have a great fascination with low level stuff, as you can probably tell by looking at my repos. I've worked on a lot of different projects and gained a lot of knowledge in a lot of niches of software.


# Repositories

Repositories tagged as `Archived` are projects that I'm not longer interested in maintaining, and will likely never be updated again. There are a handful I may revisit, but I may just start a whole new repository instead at this point.

Repositories tagged as `OLD` are old projects, created many years before uploading them here. Many of these projects are from when I was much less experienced programmer, so quality will likely be low, and some of the projects might not even build anymore. I still wanted to share them though as they are useful references, and might give others ideas or inspiration.

You can view all of my repositories [here](https://gitlab.com/users/AlphaOmegaProgrammer/projects).


## Top 5 Reposotories

[AOC 2023 in Raw Machine Code](https://gitlab.com/AlphaOmegaProgrammer/aoc-2023-raw-machine-code) - In this project, I managed to solve part 1 of three separate days of Advent of Code 2023 using nothing but an empty file full of `0x00` and a hex editor.

[SPICY](https://gitlab.com/AlphaOmegaProgrammer/spicy) - This is basically an asynchronous threaded runtime for C progams that want to do networking.

[Test Kernel](https://gitlab.com/AlphaOmegaProgrammer/test-kernel) - There was a time where I put a lot of effort into learning OSDev, and this was the most impressive thing I made back then. I know plenty more on the topic these days, and I will return to OSDev one day, but I when that day will be is unknown.

[GnuPG Open Auth](https://gitlab.com/AlphaOmegaProgrammer/gnupg-open-auth) - This was my attempt at creating an Open ID system based on GnuPG, and stored data in Postgres. I had a lot of trouble with the GnuPG C library back when I worked on this, and ended up giving up because I couldn't figure out how to use the GPG keys.

[PHP-Enabled HTTP Server](https://gitlab.com/AlphaOmegaProgrammer/old-http-server) - This was an old HTTP server I made when I was learning C and the HTTP/1.1 protocol. It's nothing too special, however it does proxy requests to php FPM using `fcgi`.

There are also some other projects that are hosted directly on [my website](https://catgirls.meme/tech/).
